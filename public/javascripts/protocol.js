const { ipcRenderer } = require('electron');


ipcRenderer.on('cmd-state', (event, arg) => {
    const textBox = document.getElementById("state");
    textBox.textContent = arg.state;
})

async function sendButton(value) {
    const textBox = document.getElementById("state");
    await ipcRenderer.send('button-input', {
       state: textBox.textContent || '',
        btn: value
    });
}
