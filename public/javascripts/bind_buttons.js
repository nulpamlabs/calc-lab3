const printable = /[0-9\+\-\*\/\.\=]/;


async function sendKeyPress(key) {
    if (printable.test(key)) {
        await sendButton(key);
    }
}

async function sendErase(full) {
    if (full) {
        await sendButton('AC');
    }
    else await sendButton('C');
}


document.addEventListener("DOMContentLoaded", function() {
    document.addEventListener('keypress', function(event) {
        console.log(event.key);
        if (event.ctrlKey && event.key === "Delete") sendErase(true);
        else if (event.key === "Delete") sendErase(false);
        else sendKeyPress(event.key);
    });
});
