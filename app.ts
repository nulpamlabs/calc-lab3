import { app, BrowserWindow, ipcMain } from 'electron';
import { dropRight, isArray } from 'lodash';
import {evaluate} from "./expression/eval";
import {ArithmeticException} from "./exceptions/exceptions";


const createWindow = async (): Promise<BrowserWindow> => {
    const win = new BrowserWindow({
        width: 400,
        height: 620,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        },
        resizable: false
    });

    return win.loadFile('public/index.html').then(_ => win);
}


app.whenReady().then(async () => {
    const win = await createWindow()

    app.on('activate', async () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            await createWindow()
        }
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})


ipcMain.on('button-input', (event, arg) => {
    let newState;
    switch (arg.btn) {
        case "C":
            newState = dropRight(arg.state, 1).join('');
            break;
        case "AC":
            newState = '';
            break;
        case "=":
            const result = evaluate(arg.state);
            if (isArray(result)) {
                console.log(result as ArithmeticException[]);
                newState = result[0].message;
            }
            else {
                newState = (result as number).toString();
            }
            break;
        default:
            newState = arg.state + arg.btn;
            break;
    }
    event.reply('cmd-state', {
        state: newState
    });
});
