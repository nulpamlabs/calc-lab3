import { findIndex, take, drop, inRange } from "lodash";


export const splitAt = <T>(idx: number, coll: Array<T>): [Array<T>, Array<T>] =>
    inRange(idx, 0, coll.length) ? [ take(coll, idx), drop(coll, idx) ] : [coll, []];


export const splitWhere = <T>(pred: (elem: T) => boolean, coll: Array<T>): [Array<T>, Array<T>] => {
    const idx = findIndex(coll, pred);
    return splitAt(idx, coll);
}
