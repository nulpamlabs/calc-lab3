export class ArithmeticException {
    readonly code: number;
    readonly index: number;
    readonly message: string;

    public constructor(code: number, index: number, message: string) {
        this.code = code;
        this.index = index;
        this.message = message;
    }
}

export const isException = <T>(value: T | ArithmeticException): value is ArithmeticException =>
    (value as ArithmeticException).code !== undefined;


export const InvalidParenthesesException = (index: number): ArithmeticException =>
    new ArithmeticException(1, index, "Invalid structure in parentheses");

export const UnknownOperatorException = (operator: string, index: number): ArithmeticException =>
    new ArithmeticException(2, index, `Unknown operator '${operator}'`);

export const InvalidExpressionException = (index: number): ArithmeticException =>
    new ArithmeticException(3, index, "Invalid expression");

export const ConsecutiveOperatorsException = (index: number): ArithmeticException =>
    new ArithmeticException(4, index, "Two consecutive operators");

export const IncompleteExpressionException = (index: number): ArithmeticException =>
    new ArithmeticException(5, index, "Incomplete expression");

export const NumberOutOfRangeException = (index: number): ArithmeticException =>
    new ArithmeticException(6, index, "Number out of range");

export const TooLargeExpressionException = (index: number): ArithmeticException =>
    new ArithmeticException(7, index, "Expression too long (max 65536)");

export const TooManyOperatorsException = (index: number): ArithmeticException =>
    new ArithmeticException(8, index, "Too many operators (max 30)");

export const ZeroDivisionException = (index: number): ArithmeticException =>
    new ArithmeticException(9, index, "Division by zero");
