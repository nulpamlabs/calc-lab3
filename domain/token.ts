import { findIndex } from "lodash";


export interface OperatorToken {
    readonly repr: string;
    readonly precedence: number;
}
export const Operator = (repr: string): OperatorToken => {
    const precedence = findIndex(precedences, (regexp) => regexp.test(repr));
    return {
        repr: repr,
        precedence: precedence
    };
}
export const isOperatorToken = (t: Token): t is OperatorToken =>
    (t as OperatorToken)?.repr !== undefined;

export interface NumberToken {
    readonly value: number;
}

export type Token = OperatorToken | NumberToken;


const precedences = [
    /[\(\)]/,
    /[\+-]/,
    /[\*\/]/,
    /^(u\-)$/
]
export const knownOperators = /[\+\-\*\/\(\)]/;
export const digits         = /[0-9]/;
export const parentheses    = /[\(\)]/;
