export interface ExprNode {
    readonly tag: string;
    readonly precedence: number;
}

export interface UnaryNode extends ExprNode {
    readonly underlying: ExprNode;
}
export const isUnaryNode = (e: ExprNode): e is UnaryNode =>
    (e as UnaryNode).underlying !== undefined;

export interface BinaryNode extends ExprNode {
    readonly left: ExprNode;
    readonly right: ExprNode;
}
export const isBinaryNode = (e: ExprNode): e is BinaryNode =>
    (e as BinaryNode).left !== undefined;

export interface NumberNode extends ExprNode {
    readonly value: number;
}
export const Number = (value: number): NumberNode => ({
    tag: 'c',
    precedence: 0,
    value: value
})
export const isNumberNode = (e: ExprNode): e is NumberNode =>
    (e as NumberNode).value !== undefined;

export const Add = (left: ExprNode, right: ExprNode): BinaryNode => ({
    tag: '+',
    precedence: 1,
    left: left,
    right: right
})

export const Subtract = (left: ExprNode, right: ExprNode): BinaryNode => ({
    tag: '-',
    precedence: 1,
    left: left,
    right: right
})

export const Parentheses = (underlying: ExprNode): UnaryNode => ({
    tag: '()',
    precedence: 5,
    underlying: underlying
})

export const Multiply = (left: ExprNode, right: ExprNode): BinaryNode => ({
    tag: '*',
    precedence: 2,
    left: left,
    right: right
})

export const Divide = (left: ExprNode, right: ExprNode): BinaryNode => ({
    tag: '/',
    precedence: 2,
    left: left,
    right: right
})

export const Negate = (underlying: ExprNode): UnaryNode => ({
    tag: 'unary-',
    precedence: 1,
    underlying: underlying
})

