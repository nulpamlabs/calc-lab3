import {
    BinaryNode,
    ExprNode,
    isBinaryNode,
    isNumberNode,
    NumberNode,
    UnaryNode
} from "../domain/expression";
import {
    ArithmeticException,
    InvalidExpressionException,
    isException,
    ZeroDivisionException
} from "../exceptions/exceptions";
import {parseExpr} from "./parser";
import {isArray} from "lodash";


const handleBinaryOp = (ast: ExprNode, comb: (l: number, r: number) => number): number | ArithmeticException => {
    const lhs = evaluateExpr((ast as BinaryNode).left);
    const rhs = evaluateExpr((ast as BinaryNode).right);
    if (isException(lhs)) {
        return lhs;
    }
    else if (isException(rhs)) {
        return rhs;
    }
    else return comb(lhs as number, rhs as number);
}


export const evaluateExpr = (ast: ExprNode): number | ArithmeticException => {
    switch (ast.tag) {
        case 'c':
            return (ast as NumberNode).value;
        case 'unary-':
            return -evaluateExpr((ast as UnaryNode).underlying);
        case '()':
            return evaluateExpr((ast as UnaryNode).underlying);
        case '+':
            return handleBinaryOp(ast, (l, r) => l + r);
        case '-':
            return handleBinaryOp(ast, (l, r) => l - r);
        case '*':
            return handleBinaryOp(ast, (l, r) => l * r);
        case '/':
            const lhs = evaluateExpr((ast as BinaryNode).left);
            const rhs = evaluateExpr((ast as BinaryNode).right);
            if (isException(lhs)) {
                return lhs;
            }
            else if (isException(rhs)) {
                return rhs;
            }
            else if ((rhs as number) === 0) {
                return ZeroDivisionException(0)
            }
            else return (lhs as number) / (rhs as number);
    }
    return InvalidExpressionException(0);
}


export const showRPN = (ast: ExprNode): string => {
    if (isNumberNode(ast)) {
        return (ast as NumberNode).value.toString();
    }
    else if (isBinaryNode(ast)) {
        return `${ast.tag} ${showRPN(ast.left)} ${showRPN(ast.right)}`;
    }
    else {
        switch (ast.tag) {
            case 'unary-':
                return `-${showRPN((ast as UnaryNode).underlying)}`;
            case '()':
                return `(${showRPN((ast as UnaryNode).underlying)})`;
        }
    }
    return '';
}


export const evaluate = (expr: string): number | ArithmeticException[] => {
    const parsed = parseExpr(expr);

    if (isArray(parsed)) {
        return parsed as ArithmeticException[];
    }
    const result = evaluateExpr(parsed as ExprNode);
    if (isException(result)) {
        return [result, ];
    }
    return result as number;
}
