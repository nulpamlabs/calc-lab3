import {
    digits,
    isOperatorToken,
    knownOperators,
    NumberToken,
    Operator,
    OperatorToken,
    parentheses,
    Token
} from "../domain/token";
import {
    ArithmeticException, ConsecutiveOperatorsException,
    IncompleteExpressionException, InvalidExpressionException,
    InvalidParenthesesException,
    isException,
    NumberOutOfRangeException, TooLargeExpressionException, TooManyOperatorsException,
    UnknownOperatorException
} from "../exceptions/exceptions";
import {splitWhere} from "../util/array";
import {drop, filter, isEmpty} from "lodash/fp";
import {BinaryNode, ExprNode, Negate, Number} from "../domain/expression";


class TokenState {
    public outputQueue: Token[];
    public operatorStack: OperatorToken[];
    public expr: string[];
    public errors: ArithmeticException[];
    public index: number;

    public constructor(expr: string[]) {
        this.outputQueue = [];
        this.operatorStack = [];
        this.expr = expr;
        this.errors = [];
        this.index = 0;
    }

    public nextState = (parseTokenRes: Token | ArithmeticException, newExpr: string[]): TokenState => {
        if (isException(parseTokenRes)) {
            this.errors.push(parseTokenRes as ArithmeticException);
        }
        else if (isOperatorToken(parseTokenRes as Token)) {
            const tokenToPush = parseTokenRes as OperatorToken;

            if (tokenToPush.repr === ')') {
                while (this.top() && this.top()?.repr !== '(') {
                    this.pop(InvalidParenthesesException);
                }
                const openingParenthesis = this.operatorStack.pop();
                if (!openingParenthesis) {
                    this.errors.push(IncompleteExpressionException(this.index));
                }
            }
            else if (tokenToPush.repr === '(') {
                this.operatorStack.push(tokenToPush);
            }
            else {
                while (tokenToPush.precedence <= (this.top()?.precedence || -1)) {
                    this.pop(IncompleteExpressionException);
                }
                this.operatorStack.push(tokenToPush);
            }
        }
        else {
            this.outputQueue.push(parseTokenRes as NumberToken);
        }
        if (isEmpty(newExpr)) {
            while (this.top()) {
                this.pop(IncompleteExpressionException);
            }
        }
        this.index += (this.expr.length - newExpr.length);
        this.expr = newExpr;
        return this;
    }

    public ready = (): boolean =>
        isEmpty(this.expr);

    public toAst = (): ExprNode | undefined | ArithmeticException[] => {
        if (!this.ready()) {
            return undefined;
        }
        const operatorsCount = filter(isOperatorToken)(this.outputQueue).length;
        if (operatorsCount > 30) {
            this.errors.push(TooManyOperatorsException(0));
        }
        if (isEmpty(this.errors)) {
            const lastNode = this.outputQueue.pop() as Token;
            return toAst(lastNode, this.outputQueue);
        } else {
            return this.errors;
        }
    }


    private pop = (exceptionConstructor: (idx: number) => ArithmeticException): TokenState => {
        if (isEmpty(this.operatorStack)) {
            this.errors.push(exceptionConstructor(this.index));
            return this;
        }
        const lastOperator = this.operatorStack.pop() as OperatorToken;
        this.outputQueue.push(lastOperator);
        return this;
    }
    private top = (): OperatorToken | undefined => {
        if (isEmpty(this.operatorStack)) {
            return undefined;
        }
        return this.operatorStack[this.operatorStack.length - 1];
    }

}


const parseNumber = (numberState: TokenState): TokenState => {

    const parseNumberToken = (index: number, asStr: string): NumberToken | ArithmeticException => {
        try {
            const number = parseFloat(asStr);
            return number > 2147483648 ? NumberOutOfRangeException(index) : { value: number } as NumberToken;
        } catch (e) {
            return NumberOutOfRangeException(index);
        }
    }

    const [tillComma, rest] = splitWhere(
        (char: string) => !digits.test(char) || char === '.',
        numberState.expr
    );
    if (rest.length > 0 && rest[0] === '.') {
        const [decimalFraction, newTailExpr] =
            splitWhere((char: string) => !digits.test(char), drop(1)(rest));

        const numberAsString = `${tillComma.join('')}.${decimalFraction.join('')}`;
        const number = parseNumberToken(numberState.index, numberAsString);

        return numberState.nextState(number, newTailExpr);
    } else {
        const number = parseNumberToken(numberState.index, tillComma.join(''));
        return numberState.nextState(number, rest);
    }
}


const parseToken = (inState: TokenState, mode: 'op'|'c'|'no-unary'): TokenState => {
    if (isEmpty(inState.expr)) {
        return inState;
    }
    const head = inState.expr[0];

    if (head === '-' && mode === 'no-unary') {
        return inState.nextState(ConsecutiveOperatorsException(inState.index), []);
    }
    if (head === '-' && mode === 'c') {
        const unaryMinusOp = Operator('u-');
        const nextState = inState.nextState(unaryMinusOp, drop(1)(inState.expr));
        return parseToken(nextState, 'no-unary');
    }
    else if (knownOperators.test(head)) {
        if ((mode === 'c' || mode === 'no-unary') && !parentheses.test(head)) {
            return inState.nextState(ConsecutiveOperatorsException(inState.index), []);
        }
        const operator = Operator(head);
        const nextState = inState.nextState(operator, drop(1)(inState.expr));
        return parseToken(nextState, parentheses.test(operator.repr) ? 'op' : 'no-unary');
    }
    else if (digits.test(head)) {
        const numberState = parseNumber(inState);
        return parseToken(numberState, 'op');
    }
    else return inState.nextState(UnknownOperatorException(head, inState.index), []);
}


const toAst = (currentToken: Token, tokenStack: Token[]): ExprNode => {
    if (isOperatorToken(currentToken) && currentToken.repr === 'u-') {
        const nextToken = tokenStack.pop() as Token;
        const underlyingNode = toAst(nextToken, tokenStack);
        return Negate(underlyingNode);
    }
    else if (isOperatorToken(currentToken)) {
        const rightToken = tokenStack.pop() as Token;
        const rightNode = toAst(rightToken, tokenStack);

        const leftToken = tokenStack.pop() as Token;
        const leftNode = toAst(leftToken, tokenStack);

        return  {
            tag: currentToken.repr,
            precedence: currentToken.precedence,
            left: leftNode,
            right: rightNode
        } as BinaryNode;
    }
    else {
        return Number((currentToken as NumberToken).value);
    }
}


export const parseExpr = (expr: string): ExprNode | ArithmeticException[] => {
    if (expr.length > 65536) {
        return [ TooLargeExpressionException(655536) ];
    }
    const strippedExpression = filter((char: string) => char !== ' ')(expr);
    return parseToken(new TokenState(strippedExpression), 'c').toAst() || [ InvalidExpressionException(0) ];
}
