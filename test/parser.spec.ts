import {Add, Divide, Multiply, Negate, Number, Parentheses, Subtract} from "../domain/expression";
import { parseExpr } from "../expression/parser";
import { expect } from 'chai';
import { isArray } from 'lodash';
import { ConsecutiveOperatorsException } from "../exceptions/exceptions";


describe('expression', () => {
    it('parses integer', () => {
        const expr = '5';
        const actual = parseExpr(expr);
        const expected = Number(5);

        expect(actual).to.eql(expected);
    });
    it('parses float', () => {
        const expr = '5.25';
        const actual = parseExpr(expr);
        const expected = Number(5.25);

        expect(actual).to.eql(expected);
    });
    it('parses negative number', () => {
        const expr = '-15';
        const actual = parseExpr(expr);
        const expected = Negate(Number(15));

        expect(actual).to.eql(expected);
    });
    it('parses expression in parentheses', () => {
        const expr = '(5)';
        const actual = parseExpr(expr);
        const expected = Number(5);

        expect(actual).to.eql(expected);
    });
    it('parses unary negation before parentheses', () => {
        const expr = '-(5)';
        const actual = parseExpr(expr);
        const expected = Negate(Number(5));

        expect(actual).to.eql(expected);
    });
    it('parses addition', () => {
        const expr = '5+2';
        const actual = parseExpr(expr);
        const expected = Add(Number(5), Number(2));

        expect(actual).to.eql(expected);
    });
    it('parses subtraction', () => {
        const expr = '18-3';
        const actual = parseExpr(expr);
        const expected = Subtract(Number(18), Number(3));

        expect(actual).to.eql(expected);
    });
    it('parses multiplication', () => {
        const expr = '10*11';
        const actual = parseExpr(expr);
        const expected = Multiply(Number(10), Number(11));

        expect(actual).to.eql(expected);
    });
    it('parses division', () => {
        const expr = '5/3';
        const actual = parseExpr(expr);
        const expected = Divide(Number(5), Number(3));

        expect(actual).to.eql(expected);
    });
    it('parses complex expression', () => {
        const expr = '15-2*3/4';
        const actual = parseExpr(expr);
        const expected = Subtract(
            Number(15),
            Divide(
                Multiply(
                    Number(2),
                    Number(3)
                ),
                Number(4)
            )
        );

        expect(actual).to.eql(expected);
    });
    it('parses complex expression with parentheses', () => {
        const expr = '(22/8+11)-(3+2)/4';
        const actual = parseExpr(expr);
        const expected = Subtract(
            Add(
                Divide(
                    Number(22),
                    Number(8)
                ),
                Number(11)
            ),
            Divide(
                Add(
                    Number(3),
                    Number(2)
                ),
                Number(4)
            )
        );

        expect(actual).to.eql(expected);
    });
    it('produces ConsecutiveOperatorsException on consecutive unary minuses', () => {
        const expr = '8---8';
        const actual = parseExpr(expr);
        const expected = ConsecutiveOperatorsException(2);

        expect(actual).to.satisfy(isArray).and.to.to.deep.include.members([expected]);
    });
});
