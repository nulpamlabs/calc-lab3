import { expect } from 'chai';
import { evaluate } from "../expression/eval";


describe('expression', () => {
    it('evaluates integer', () => {
        const expr = '5';
        const actual = evaluate(expr);
        const expected = 5;

        expect(actual).to.eql(expected);
    });
    it('evaluates float', () => {
        const expr = '5.25';
        const actual = evaluate(expr);
        const expected = 5.25;

        expect(actual).to.eql(expected);
    });
    it('evaluates negative number', () => {
        const expr = '-15';
        const actual = evaluate(expr);
        const expected = -15;

        expect(actual).to.eql(expected);
    });
    it('evaluates expression in parentheses', () => {
        const expr = '(5)';
        const actual = evaluate(expr);
        const expected = 5;

        expect(actual).to.eql(expected);
    });
    it('evaluates unary negation before parentheses', () => {
        const expr = '-(5)';
        const actual = evaluate(expr);
        const expected = -5;

        expect(actual).to.eql(expected);
    });
    it('evaluates addition', () => {
        const expr = '5+2';
        const actual = evaluate(expr);
        const expected = 7;

        expect(actual).to.eql(expected);
    });
    it('evaluates subtraction', () => {
        const expr = '18-3';
        const actual = evaluate(expr);
        const expected = 15;

        expect(actual).to.eql(expected);
    });
    it('evaluates multiplication', () => {
        const expr = '10*11';
        const actual = evaluate(expr);
        const expected = 110;

        expect(actual).to.eql(expected);
    });
    it('evaluates division', () => {
        const expr = '5/2';
        const actual = evaluate(expr);
        const expected = 2.5;

        expect(actual).to.eql(expected);
    });
    it('evaluates complex expression', () => {
        const expr = '15-2*3/4';
        const actual = evaluate(expr);
        const expected = 13.5

        expect(actual).to.eql(expected);
    });
    it('evaluates complex expression with parentheses', () => {
        const expr = '(22/8+11)-(3+2)/4';
        const actual = evaluate(expr);
        const expected = (22/8+11)-5/4

        expect(actual).to.eql(expected);
    });
    it('evaluates many subtractions', () => {
        const expr = '2 - 5 - 2 + 1';
        const actual = evaluate(expr);
        const expected = 2 - 5 - 2 + 1;

        expect(actual).to.eql(expected);
    });
    it('evaluates a really complex expression', () => {
        const expr = '(3*3+4*((22/8+11)-(3+2)/4+15-2*3/(4/2/2)))*4';
        const actual = evaluate(expr);
        const expected = 380;

        expect(actual).to.eql(expected);
    })

});
